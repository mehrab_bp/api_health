package service

import (
	"context"
	"errors"
	"fmt"
	"vafa-health/internal/domain/model"
	"vafa-health/internal/repository"
)

type IApiService interface {
	Create(ctx context.Context, api model.Api) (*model.Api, error)
	GetAll(ctx context.Context) ([]*model.Api, error)
	Delete(ctx context.Context, id int) error
}

type ApiService struct {
	apiRepo           repository.IApiRepository
	apiCheckerManager IApiCheckerManager
}

func NewApiService(apiRepo repository.IApiRepository, apiCheckManager IApiCheckerManager) IApiService {
	return &ApiService{
		apiRepo:           apiRepo,
		apiCheckerManager: apiCheckManager,
	}
}

func (a ApiService) Create(ctx context.Context, api model.Api) (*model.Api, error) {
	return a.apiRepo.Create(ctx, api)
}

func (a ApiService) GetAll(ctx context.Context) ([]*model.Api, error) {
	return a.apiRepo.GetAll(ctx)
}

func (a ApiService) Delete(ctx context.Context, id int) error {
	api, err := a.apiRepo.Get(ctx, id)
	if err != nil {
		return errors.New(fmt.Sprintf("there is no api with id %d", id))
	}

	tx, err := a.apiRepo.Delete(ctx, id)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = a.apiCheckerManager.Remove(api)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}
