package service

import (
	"context"
	"errors"
	"fmt"
	"vafa-health/internal/domain/model"
	"vafa-health/internal/repository"
)

type IApiCheckerManager interface {
	Initial(ctx context.Context)
	Start(ctx context.Context, id int) error
	Stop(ctx context.Context, id int) error
	Remove(api *model.Api) error
}

type ApiCheckerManager struct {
	apis          map[int]*model.ApiChecker
	apiRepo       repository.IApiRepository
	apiLogService IApiStatusLogService
}

func NewApiCheckerManager(apiRepo repository.IApiRepository, apiLogService IApiStatusLogService) *ApiCheckerManager {
	return &ApiCheckerManager{
		apis:          make(map[int]*model.ApiChecker),
		apiRepo:       apiRepo,
		apiLogService: apiLogService,
	}
}

func (acm *ApiCheckerManager) Initial(ctx context.Context) {
	apis, err := acm.apiRepo.GetAllStarted(ctx)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, api := range apis {
		apiChecker := model.NewApiChecker(*api)
		acm.apis[api.ID] = apiChecker
		acm.StartTicker(acm.apis[api.ID])
	}
}

func (acm *ApiCheckerManager) Start(ctx context.Context, id int) error {
	api, err := acm.apiRepo.Get(ctx, id)
	if err != nil {
		return errors.New(fmt.Sprintf("there is no api with id %d", id))
	}
	if api.Status == model.Started {
		return errors.New("api already is started")
	}

	tx, err := acm.apiRepo.UpdateStatus(ctx, id, model.Started)
	if err != nil {
		tx.Rollback()
		return err
	}

	if _, ok := acm.apis[id]; ok {
		acm.StartTicker(acm.apis[id])
	} else {
		apiChecker := model.NewApiChecker(*api)
		acm.apis[id] = apiChecker
		acm.StartTicker(apiChecker)
	}
	tx.Commit()

	return nil
}

func (acm *ApiCheckerManager) StartTicker(apiChecker *model.ApiChecker) {
	ctx := context.Background()
	go func() {
		for {
			select {
			case <-apiChecker.StopCh:
				fmt.Println("Worker: received stop signal")
				return
			case <-apiChecker.Ticker.C:
				status, err := apiChecker.CheckURL()
				if err != nil {
					fmt.Println(err)
				} else {
					err := acm.apiLogService.Create(ctx, &apiChecker.Api, status)
					if err != nil {
						fmt.Println(err)
					}
				}
			}
		}
	}()
}

func (acm *ApiCheckerManager) Stop(ctx context.Context, id int) error {
	api, err := acm.apiRepo.Get(ctx, id)
	if err != nil {
		return errors.New(fmt.Sprintf("there is no api with id %d", id))
	}
	if api.Status != model.Started {
		return nil
	}

	tx, err := acm.apiRepo.UpdateStatus(ctx, id, model.Stopped)
	if err != nil {
		tx.Rollback()
		return err
	}

	if apiChecker, ok := acm.apis[id]; ok {
		acm.StopTicker(apiChecker)
		delete(acm.apis, api.ID)
	} else {
		tx.Rollback()
		return errors.New("the api is not started yet")
	}
	tx.Commit()

	return nil
}

func (acm *ApiCheckerManager) StopTicker(apiChecker *model.ApiChecker) {
	close(apiChecker.StopCh)
}

func (acm *ApiCheckerManager) Remove(api *model.Api) error {
	if api.Status == model.Started {
		if apiChecker, ok := acm.apis[api.ID]; ok {
			acm.StopTicker(apiChecker)
		}
	}

	delete(acm.apis, api.ID)
	return nil
}
