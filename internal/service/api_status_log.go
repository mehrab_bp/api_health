package service

import (
	"context"
	"net/http"
	"time"
	"vafa-health/internal/domain/model"
	"vafa-health/internal/repository"
)

type IApiStatusLogService interface {
	Create(ctx context.Context, api *model.Api, status int) error
}

type ApiStatusLogService struct {
	apiLogRepo repository.IApiStatusLogRepository
}

func NewApiStatusLogService(apiLogRepo repository.IApiStatusLogRepository) IApiStatusLogService {
	return &ApiStatusLogService{
		apiLogRepo: apiLogRepo,
	}
}

func (a ApiStatusLogService) Create(ctx context.Context, api *model.Api, status int) error {
	err := a.apiLogRepo.Create(ctx, model.ApiStatusLog{
		ApiId:          api.ID,
		ResponseStatus: status,
		CreatedAt:      time.Now(),
	})

	if status != http.StatusOK {
		go func() {
			//call somewhere
		}()
	}

	return err
}
