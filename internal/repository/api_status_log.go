package repository

import (
	"context"
	"gorm.io/gorm"
	"vafa-health/internal/domain/model"
)

type IApiStatusLogRepository interface {
	Create(ctx context.Context, log model.ApiStatusLog) error
}

type ApiStatusLogRepository struct {
	db *gorm.DB
}

func NewApiStatusLogRepository(db *gorm.DB) IApiStatusLogRepository {
	return &ApiStatusLogRepository{db: db}
}

func (a ApiStatusLogRepository) Create(ctx context.Context, log model.ApiStatusLog) error {
	err := a.db.WithContext(ctx).Table("api_status_logs").Create(&log).Error
	if err != nil {
		return err
	}

	return nil
}
