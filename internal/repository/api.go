package repository

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
	"vafa-health/internal/domain/model"
)

type IApiRepository interface {
	Create(ctx context.Context, api model.Api) (*model.Api, error)
	Get(ctx context.Context, id int) (*model.Api, error)
	GetAll(ctx context.Context) ([]*model.Api, error)
	GetAllStarted(ctx context.Context) ([]*model.Api, error)
	UpdateStatus(ctx context.Context, id int, status model.ApiStatus) (*gorm.DB, error)
	Delete(ctx context.Context, id int) (*gorm.DB, error)
}

type ApiRepository struct {
	db *gorm.DB
}

func NewApiRepository(db *gorm.DB) IApiRepository {
	return &ApiRepository{db: db}
}

func (a ApiRepository) Create(ctx context.Context, api model.Api) (*model.Api, error) {
	err := a.db.WithContext(ctx).Table("apis").Create(&api).Error
	if err != nil {
		return nil, err
	}

	return &api, nil
}

func (a ApiRepository) Get(ctx context.Context, id int) (*model.Api, error) {
	var api *model.Api
	err := a.db.WithContext(ctx).
		Table("apis").
		Where("id = @id", sql.Named("id", id)).
		First(&api).
		Error

	if err != nil {
		return nil, err
	}
	return api, nil
}

func (a ApiRepository) GetAll(ctx context.Context) ([]*model.Api, error) {
	var apis []*model.Api

	err := a.db.WithContext(ctx).
		Table("apis").
		Find(&apis).
		Error
	if err != nil {
		return nil, err
	}

	return apis, nil
}

func (a ApiRepository) GetAllStarted(ctx context.Context) ([]*model.Api, error) {
	var apis []*model.Api

	err := a.db.WithContext(ctx).
		Table("apis").
		Where("status = @status", sql.Named("status", "checking_started")).
		Find(&apis).
		Error
	if err != nil {
		return nil, err
	}

	return apis, nil
}

func (a ApiRepository) UpdateStatus(ctx context.Context, id int, status model.ApiStatus) (*gorm.DB, error) {
	tx := a.db.Begin()

	err := tx.WithContext(ctx).
		Table("apis").
		Where("id = @id", sql.Named("id", id)).
		Update("status", status).
		Error

	return tx, err
}

func (a ApiRepository) Delete(ctx context.Context, id int) (*gorm.DB, error) {
	tx := a.db.Begin()

	err := tx.WithContext(ctx).
		Table("apis").
		Where("id = @id", sql.Named("id", id)).
		Delete(&model.Api{}).
		Error

	return tx, err
}
