package model

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

type ApiChecker struct {
	Api    Api
	Ticker *time.Ticker
	StopCh chan struct{}
}

func NewApiChecker(api Api) *ApiChecker {
	intervalDuration := time.Duration(api.HealthCheckInterval) * time.Minute
	return &ApiChecker{
		Api:    api,
		Ticker: time.NewTicker(intervalDuration),
		StopCh: make(chan struct{}),
	}
}

func (ac *ApiChecker) CheckURL() (int, error) {
	var request *http.Request
	var err error
	if ac.Api.Body != "" {
		request, err = http.NewRequest(ac.Api.HttpMethod, ac.Api.Url,
			bytes.NewBuffer([]byte(ac.Api.Body)))
	} else {
		request, err = http.NewRequest(ac.Api.HttpMethod, ac.Api.Url,
			nil)
	}
	if err != nil {
		return 0, err
	}

	headers := make(map[string]string)
	if ac.Api.Headers != "" {
		if err := json.Unmarshal([]byte(ac.Api.Headers), &headers); err != nil {
			return 0, err
		}
	}
	for key, value := range headers {
		request.Header.Set(key, value)
	}

	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return 0, err
	}

	return response.StatusCode, nil
}
