package model

import "time"

type ApiStatusLog struct {
	ID             int       `gorm:"column:id"`
	ApiId          int       `gorm:"column:api_id"`
	ResponseStatus int       `gorm:"column:response_status"`
	CreatedAt      time.Time `gorm:"column:created_at"`
}

func (ApiStatusLog) TableName() string {
	return "api_status_logs"
}
