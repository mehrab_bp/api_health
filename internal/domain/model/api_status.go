package model

type ApiStatus string

const (
	NoAction ApiStatus = "no_action"
	Started  ApiStatus = "checking_started"
	Stopped  ApiStatus = "checking_stopped"
)
