package model

type Api struct {
	ID                  int       `json:"id" gorm:"column:id"`
	HealthCheckInterval int       `json:"health_check_interval" gorm:"column:health_check_interval"`
	Url                 string    `gorm:"column:url"`
	HttpMethod          string    `json:"http_method" gorm:"column:http_method"`
	Headers             string    `gorm:"column:headers"`
	Body                string    `gorm:"column:body"`
	Status              ApiStatus `gorm:"column:status"`
}

func (Api) TableName() string {
	return "apis"
}
