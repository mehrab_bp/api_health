package controller

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"vafa-health/internal/domain/model"
	"vafa-health/internal/service"
)

type IApiController interface {
	Create(c *gin.Context)
	Start(c *gin.Context)
	Stop(c *gin.Context)
	GetAll(c *gin.Context)
	Delete(c *gin.Context)
}

type ApiController struct {
	apiService        service.IApiService
	apiCheckerManager service.IApiCheckerManager
}

func NewApiController(apiService service.IApiService, apiCheckerManager service.IApiCheckerManager) IApiController {
	return &ApiController{
		apiService:        apiService,
		apiCheckerManager: apiCheckerManager,
	}
}

func (a ApiController) Create(c *gin.Context) {
	var api model.Api
	if err := c.ShouldBind(&api); err != nil {
		err = errors.New("could not parse api")
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Could not parse body"})
		return
	}
	api.Status = model.NoAction

	createdApi, err := a.apiService.Create(c.Request.Context(), api)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal server error occurred"})
		return
	}

	c.AbortWithStatusJSON(http.StatusOK, gin.H{"response": fmt.Sprintf("api created with id %d", createdApi.ID)})
}

func (a ApiController) Start(c *gin.Context) {
	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	err = a.apiCheckerManager.Start(c.Request.Context(), id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.AbortWithStatusJSON(http.StatusOK, gin.H{"response": fmt.Sprintf("api started with id %d", id)})
}

func (a ApiController) Stop(c *gin.Context) {
	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	err = a.apiCheckerManager.Stop(c.Request.Context(), id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.AbortWithStatusJSON(http.StatusOK, gin.H{"response": fmt.Sprintf("api stopped with id %d", id)})
}

func (a ApiController) GetAll(c *gin.Context) {
	apis, err := a.apiService.GetAll(c.Request.Context())
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "internal server error occurred"})
		return
	}

	c.AbortWithStatusJSON(http.StatusOK, apis)
}

func (a ApiController) Delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	err = a.apiService.Delete(c.Request.Context(), id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.AbortWithStatusJSON(http.StatusOK, gin.H{"response": fmt.Sprintf("api deleted with id %d", id)})
}
