package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"vafa-health/cmd/connection"
	"vafa-health/internal/controller"
	"vafa-health/internal/repository"
	"vafa-health/internal/service"
)

func main() {
	ctx, cnl := context.WithCancel(context.Background())
	defer cnl()

	dbClient, mysqlDB := connection.MysqlConnection()

	apiRepo := repository.NewApiRepository(dbClient)
	apiStatusLogRepo := repository.NewApiStatusLogRepository(dbClient)

	apiStatusLogService := service.NewApiStatusLogService(apiStatusLogRepo)
	apiCheckerService := service.NewApiCheckerManager(apiRepo, apiStatusLogService)
	apiService := service.NewApiService(apiRepo, apiCheckerService)

	apiController := controller.NewApiController(apiService, apiCheckerService)

	go func() {
		apiCheckerService.Initial(ctx)
	}()

	router := gin.Default()
	router.Use(gin.Recovery())

	router.GET("/apis", apiController.GetAll)
	router.POST("/", apiController.Create)
	router.DELETE("/", apiController.Delete)
	router.POST("/start", apiController.Start)
	router.POST("/stop", apiController.Stop)

	router.Run(":8080")

	go func() {
		<-ctx.Done()

		//Shutting down MySql
		if err := mysqlDB.Close(); err != nil {
			log.Fatal("failed to close mysql", err)
		}
	}()
}
