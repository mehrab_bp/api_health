package connection

import (
	"database/sql"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
	"log"
	"time"
)

const (
	Host     = "127.0.0.1"
	Port     = 3306
	User     = "vafa"
	Password = "vafa"
	DBName   = "api_health"
)

func MysqlConnection() (*gorm.DB, *sql.DB) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		User, Password, Host, Port, DBName)

	fmt.Println(fmt.Sprintf("connecting to mysql: %s %d %s", Host, Port, DBName))

	gormDb, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger:      gormLogger.Default.LogMode(gormLogger.Info),
		PrepareStmt: true,
	})

	if err != nil {
		log.Fatal(fmt.Sprintf("failed to connect to mysql: %s", dsn))
	}

	sqlDb, err := gormDb.DB()
	if err != nil {
		log.Fatal(fmt.Sprintf("failed to get sqldb: %s", dsn))
	}

	sqlDb.SetMaxIdleConns(50)
	sqlDb.SetMaxOpenConns(100)
	sqlDb.SetConnMaxLifetime(time.Hour)

	fmt.Println(fmt.Sprintf("connected to mysql: %s %d %s", Host, Port, DBName))

	return gormDb, sqlDb
}
