CREATE TABLE apis (
       id INT AUTO_INCREMENT PRIMARY KEY,
       health_check_interval INT,
       url VARCHAR(255),
       http_method VARCHAR(10),
       headers TEXT,
       body TEXT,
       status    enum ('no_action', 'checking_started', 'checking_stopped')
);

CREATE TABLE api_status_logs (
     id INT AUTO_INCREMENT PRIMARY KEY,
     api_id INT not null ,
     response_status INT not null ,
     created_at timestamp  NOT NULL,
    FOREIGN KEY (api_id) REFERENCES apis (id)
);